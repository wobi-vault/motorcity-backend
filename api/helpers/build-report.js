const nodemailer = require('nodemailer');
module.exports = {

  description: 'Build excel file according lead data.',

  inputs: {
    leads: {
      type: 'ref',
      description: 'The lead data to build excel.',
      required: true
    },
    columns: {
      type: 'ref',
      description: 'The columns for the excel headers.',
      required: true
    }
  },

  exits: {
    success: {},
    error: {}
  },

  fn: async (inputs, exits) => {
    try {
      const xl = require('excel4node');
      const asyncJS = require('async');
      var wb = new xl.Workbook();
      var ws = wb.addWorksheet('Sheet 1');

      const leadIdCol = 'Lead id';
      const limit = 100;

      let columns = inputs.columns;
      columns.push(leadIdCol);
      columns = _.uniq(columns);

      _.each(columns, (element, index) => {
        ws.cell(1, index + 1).string(element);
      });

      asyncJS.eachOfLimit(
        inputs.leads,
        limit,
        (lead, index, cbb) => {
          _.each(columns, (e, i) => {
            if (!_.isEqual(typeof lead[e], 'string')) {
              if (_.isEqual(typeof lead[e], 'boolean')) {
                lead[e] = lead[e] ? 'Sí' : 'No';
              } else {
                lead[e] = lead[e].toLocaleString('es');
              }
            }
            ws.cell(index + 2, i + 1).string(lead[e] || '');
          });
          setTimeout(cbb, 0);
        },
        () => {
          wb.write(sails.config.report.path);
          console.log('Excel completed');
          var response = {
            count: inputs.leads.length,
            path: sails.config.report.path
          };

          return exits.success(response);
        }
      );

    } catch (error) {
      console.log(error);
      return exits.error(error);
    }
  }
};
