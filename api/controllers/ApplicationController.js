/**
 * ApplicationController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  filterResult: async (req, res) => {
    const params = req.allParams();
    try {
      const application = await Application.findOne({
        where: {
          id: params.applicationId,
        },
        include: [{
          model: VehicleFeature,
          as: 'vehicleFeatures',
        }]
      });

      let normalVehicles = await Vehicle.findAll({
        where: {
          active: true
        },
        include: [{
            model: VehicleType,
            as: 'vehicleTypes',
            where: {
              id: application.vehicleTypeId,
            },
          }, {
            model: VehicleFeature,
            as: 'vehicleFeatures',
            where: {
              id: {
                [Sequelize.Op.in]: _.pluck(application.vehicleFeatures, 'id')
              },
            },
          }, {
            model: VehicleSize,
            as: 'vehicleSizes',
            where: {
              id: application.vehicleSizeId,
            },
          }, {
            model: VehicleMaxPrice,
            as: 'vehicleMaxPrices',
            where: {
              id: application.vehicleMaxPriceId,
            },
          },
          // {
          //   model: MonthlyPayment,
          //   as: 'vehiclePayments',
          //   where: {
          //     id: application.monthlyPaymentId,
          //   },
          // },
          {
            model: Picture,
            as: 'pictures',
          },
          {
            model: Urls,
            as: 'vehicleUrls',
          }
        ],
      });

      return res.ok({
        vehicles: normalVehicles
      });
    } catch (e) {
      return res.negotiate(e);
    }
  },

  vehicleFeature: async (req, res) => {
    const params = req.allParams();
    try {
      let prevAnswers = await ApplicationVehicleFeature.findOne({
        where: {
          applicationId: params.applicationId,
        }
      });
      if (prevAnswers) {
        ApplicationVehicleFeature.destroy({
          where: {
            applicationId: params.applicationId,
          }
        });
      }
      _.each(params.answers, async (ans) => {
        await ApplicationVehicleFeature.create({
          applicationId: params.applicationId,
          vehicleFeatureId: ans
        });
      });
      return res.ok();
    } catch (e) {
      return res.negotiate(e);
    }
  },

  vehiclePurpose: async (req, res) => {
    const params = req.allParams();
    try {
      let prevAnswers = await ApplicationVehiclePurpose.findOne({
        where: {
          applicationId: params.applicationId,
        }
      });
      if (prevAnswers) {
        ApplicationVehiclePurpose.destroy({
          where: {
            applicationId: params.applicationId,
          }
        });
      }
      _.each(params.answers, async (ans) => {
        await ApplicationVehiclePurpose.create({
          applicationId: params.applicationId,
          vehiclePurposeId: ans
        });
      });
      return res.ok();
    } catch (e) {
      return res.negotiate(e);
    }
  },

  scoreAnswer: async (req, res) => {
    var params = req.allParams();

    try {
      let applicationQuestion = await ApplicationQuestion.findOne({
        where: {
          applicationId: params.applicationId,
          questionId: params.questionId
        }
      });

      if (applicationQuestion) {
        ApplicationQuestionScore.destroy({
          where: {
            applicationQuestionId: applicationQuestion.id
          }
        });
      } else {
        applicationQuestion = await ApplicationQuestion.create({
          applicationId: params.applicationId,
          questionId: params.questionId
        });
      }

      let newAnswer = await ApplicationQuestionScore.create({
        applicationQuestionId: applicationQuestion.id,
        scoreAnswerId: params.scoreAnswerId
      });

      let answeredQuestions = await ApplicationQuestion.findAll({
        where: {
          applicationId: params.applicationId
        },
        include: [{
          model: ScoreAnswer,
          as: 'scoreAnswers',
          include: [{
            model: Score,
            as: 'score'
          }]
        }]
      });

      let application = await Application.findByPk(params.applicationId);
      let scoreQuestions = await Question.count({
        where: {
          active: true,
          questionTypeId: 1,
          vehicleTypeId: application.vehicleTypeId
        }
      });

      let scoreSum = _.reduce(answeredQuestions, (mem, answeredQuestion) => {
        return mem + _.reduce(answeredQuestion.scoreAnswers, (memo, num) => {
          return memo + num.score.score;
        }, 0);
      }, 0);

      application.update({
        partialScore: scoreSum / answeredQuestions.length,
        finalScore: scoreSum / scoreQuestions
      }).then(() => {});

      return res.ok();
    } catch (e) {
      return res.negotiate(e);
    }
  },

  categoryAnswer: async (req, res) => {
    var params = req.allParams();

    try {
      let applicationQuestion = await ApplicationQuestion.findOne({
        where: {
          applicationId: params.applicationId,
          questionId: params.questionId
        }
      });

      if (applicationQuestion) {
        ApplicationQuestionCategory.destroy({
          where: {
            applicationQuestionId: applicationQuestion.id
          }
        });
      } else {
        applicationQuestion = await ApplicationQuestion.create({
          applicationId: params.applicationId,
          questionId: params.questionId
        });
      }

      _.each(params.categoryAnswers, async (categoryAnswer) => {
        let newAnswer = await ApplicationQuestionCategory.create({
          applicationQuestionId: applicationQuestion.id,
          categoryAnswerId: categoryAnswer
        });
      });

      return res.ok();
    } catch (e) {
      return res.negotiate(e);
    }
  }

};