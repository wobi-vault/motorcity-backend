/**
 * PictureController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const options = { // This is the usual stuff
  adapter: require('skipper-better-s3'),
  key: sails.config.awsCredentials.accessKeyId,
  secret: sails.config.awsCredentials.secretAccessKey,
  bucket: sails.config.awsCredentials.bucket,
  s3params: {
    ACL: 'public-read'
  }
}

module.exports = {

  upload: (req, res) => {
    let params = req.allParams();

    try {
      req.file('picture').upload(options, async (err, files) => {
        if (err) {
          return res.negotiate(err);
        }
        let picture = await Picture.create({
          name: files[0].filename,
          location: files[0].extra.Location,
          contentType: files[0].type,
          vehicleId: params.vehicleId
        });
        return res.ok(picture);
      });
    } catch (e) {
      return res.negotiate(e);
    }
  }

};