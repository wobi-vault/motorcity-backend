/**
 * QuestionController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {

  find: async (req, res) => {
    let params = req.allParams();
    params.active = true;

    try {
      let questions = await Question.findAll({
        where: params,
        include: [{
          model: ScoreAnswer,
          as: 'scoreAnswers',
          where: {
            active: true
          },
          required: false
        }, {
          model: CategoryAnswer,
          as: 'categoryAnswers',
          where: {
            active: true
          },
          required: false
        }],
        order: [
          [{
            model: ScoreAnswer,
            as: 'scoreAnswers'
          }, 'id'],
          [{
            model: CategoryAnswer,
            as: 'categoryAnswers'
          }, 'id']
        ]
      });
      return res.ok(questions);
    } catch (e) {
      return res.negotiate(e);
    }
  }

};