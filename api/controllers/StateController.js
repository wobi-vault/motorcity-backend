/**
 * StateController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  find: async (req, res) => {
    let params = req.allParams();
    let order = null;
    if (params.order) {
      order = params.order.split(',');
      order = _.map(order, (o) => o.split(':'));
    }

    params = _.omit(params, 'order',);

    try {
      let states = await State.findAll({
        where: params,
        order,
      });
      return res.ok(states);
    } catch (e) {
      return res.negotiate(e);
    }
  },
};
