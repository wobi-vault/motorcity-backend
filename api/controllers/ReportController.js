/**
 * ReportController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const fs = require('fs');
const zeroFill = require('zero-fill');
const zerosAtLeft = 8;
const autofinId = 1;

module.exports = {
  step2Lead: (req, res) => {
    let params = req.allParams();
    let income = '';
    let questions = '';
    let vehicles = '';
    let recommendedLabel = 'Recomendados:';

    try {
      const step2Template = 'api/templates/step2-leads.html';
      const resultsTemplate = 'api/templates/results-leads.html';
      const template =  params.step2 ? step2Template : resultsTemplate;
      fs.readFile(
        template,
        'utf8',
        async (err, content) => {
          if (err) {
            return res.negotiate(err);
          }

          let application = await Application.findByPk(params.applicationId, {
            include: [
              {
                model: Lead,
                as: 'lead'
              },
              {
                model: VehicleType,
                as: 'vehicleType'
              }
            ]
          });

          let onlyOne = await Lead.count({
            where: {
              email: application.lead.email,
              phone: application.lead.phone
            }
          });

          if (onlyOne > 1) {
            return res.ok({}, {
              message: 'Message not sent due to duplicate Lead.'
            });
          }

          if (params.income === undefined) {
            const Sequelize = require('sequelize');
            const dbCredentials = sails.config.datastores.default;
            const sequelize = new Sequelize(dbCredentials.database, dbCredentials.user, dbCredentials.password, {
              host: dbCredentials.options.host,
              port: dbCredentials.options.port,
              dialect: dbCredentials.options.dialect
            });
            const COLUMNS = '"Pregunta", "Respuesta de categoría", "Respuesta de puntuación"';
            const QUERY_VIEW = `SELECT ${COLUMNS} FROM public.full_lead WHERE "Lead id"=${application.lead.id}`;
            let rows = await sequelize.query(QUERY_VIEW, {
              type: sequelize.QueryTypes.SELECT
            });

            let previewQuestion = '';
            _.each(rows, row => {
              let aux = row['Pregunta'];
              if (aux !== previewQuestion) {
                questions += `${aux}<br />`;
                previewQuestion = aux;
              }
              if (row['Respuesta de categoría'] !== null) {
                questions += `<li>${row['Respuesta de categoría']}</li>`;
              } else {
                questions += `<li>${row['Respuesta de puntuación']}</li>`;
              }
            });

            _.each(params.vehicles, vehicle => {
              vehicles += `${vehicle.model}<br />`;
            });

            if (params.lowCost) {
              recommendedLabel = 'Recomendados (por incompatibilidad de estilos):';
            }
          } else {
            income = await ScoreAnswer.findByPk(params.income);
          }

          let message = content;
          message = message.replace(
            '{{ id }}',
            zeroFill(zerosAtLeft, application.lead.id)
          );
          message = message.replace('{{ name }}', application.lead.name);
          message = message.replace('{{ phone }}', application.lead.phone);
          message = message.replace('{{ email }}', application.lead.email);
          message = message.replace(
            '{{ vehicleType }}',
            application.vehicleType.type
          );
          message = message.replace('{{ income }}', income.answer);
          message = message.replace('{{ recommended }}', recommendedLabel);
          message = message.replace('{{ vehicles }}', vehicles);
          message = message.replace('{{ questions }}', questions);

          let emails = await Email.findAll({
            where: {
              companyId: autofinId,
              active: true
            }
          });

          emails = _.map(emails, email => {
            return email.email;
          });

          let inputs = {
            subject: 'Nuevo lead de MotorCity',
            message: message,
            to: emails
          };

          let email = await sails.helpers.sendEmail.with(inputs);

          return res.ok();
        }
      );
    } catch (e) {
      return res.negotiate(e);
    }
  },

  email: async (req, res) => {
    let params = req.allParams();
    try {
      if (!params.startDate || !params.endDate) {
        return res.badRequest({}, {
          message: 'One or more params missing.'
        });
      }

      let startDate = new Date(params.startDate);
      let endDate = new Date(params.endDate);
      let columns = params.columns || sails.config.report.columns;

      if (!startDate.getDate() || !endDate.getDate()) {
        return res.badRequest({}, {
          message: 'Not a valid date.'
        });
      }

      let leads = await sails.helpers.getReportData(params.startDate, params.endDate, columns, params.repeated);
      let report = await sails.helpers.buildReport(leads, columns);
      let message = `Reporte del ${startDate.toLocaleString('es')}
        al ${endDate.toLocaleString('es')}.
        Total de leads nuevos: ${report.count}.`;
      if (!params.repeated) {
        message =
            message +
            '\n Nota: El reporte omite los leads repetidos, basado en su correo electrónico y teléfono.';
      }

      let attachment = (report.count) ? [report.path] : null;

      let inputs = {
        subject: sails.config.report.subject,
        message: message,
        to: sails.config.report.to,
        attachments: attachment
      };

      await sails.helpers.sendEmail.with(inputs);
      console.log('Email sended');

      await Report.create({
        title: params.title || sails.config.report.title || '',
        subject: sails.config.report.subject,
        hour: new Date().getHours(),
        columns: columns.join(','),
        repeatedData: params.repeated,
        emails: sails.config.report.to.join(','),
        startDate: params.startDate,
        endDate: params.endDate,
      });

      return res.ok(leads);

    } catch (e) {
      return res.negotiate(e);
    }
  },

  resend: async (req, res) => {
    let params = req.allParams();

    try {
      let report = await Report.findByPk(params.id);
      let columns = report.columns.split(',').map(c => {
        return c.trim();
      });
      let startDate = new Date(report.startDate);
      let endDate = new Date(report.endDate);

      let leads = await sails.helpers.getReportData(report.startDate, report.endDate, columns, report.repeatedData);
      let reportDoc = await sails.helpers.buildReport(leads, columns);
      let message = `Reporte del ${startDate.toLocaleString('es')}
      al ${endDate.toLocaleString('es')}.
      Total de leads nuevos: ${reportDoc.count}.`;
      if (!report.repeatedData) {
        message =
          message +
          '\n Nota: El reporte omite los leads repetidos, basado en su correo electrónico y teléfono.';
      }

      let attachment = (reportDoc.count) ? [reportDoc.path] : null;

      let inputs = {
        subject: sails.config.report.subject,
        message: message,
        to: report.emails,
        attachments: attachment
      };
      await sails.helpers.sendEmail.with(inputs);
      console.log('Email sended');
      fs.unlink(reportDoc.path, err => {
        if (err) {
          console.log(reportDoc.path, ' wasn\'t deleted');
        } else {
          console.log(reportDoc.path, ' was deleted');
        }
      });

      return res.ok(leads);

    } catch (e) {
      return res.negotiate(e);
    }
  },

  columns: (req, res) => {
    return res.ok(sails.config.report.columns);
  }
};
