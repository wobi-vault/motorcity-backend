/**
 * LeadController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {

  create: async (req, res) => {
    var params = req.allParams();
    var lead = await Lead.create(params);
    params.leadId = lead.id;
    var application = await Application.create(params);
    lead.dataValues.application = application;
    res.ok(lead);
  }

};