/**
 * VehicleController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const Op = Sequelize.Op;

module.exports = {

  find: async (req, res) => {
    let params = req.allParams();
    let order = null;
    if (params.order) {
      order = params.order.split(',');
      order = _.map(order, (o) => o.split(':'));
    }

    params = _.omit(params, 'order', );

    try {
      let vehicles = await Vehicle.findAll({
        order,
        where: params,
        include: [{
            model: Picture,
            as: 'pictures',
          },
          {
            model: Urls,
            as: 'vehicleUrls',
          }
        ],
      });
      return res.ok(vehicles);
    } catch (e) {
      return res.negotiate(e);
    }
  },

  fromApplication: async (req, res) => {
    let params = req.allParams();
    const sedan = 'Sedán';
    const hatchback = 'Hatchback';
    const bikeId = 3;

    try {
      let application = await Application.findByPk(params.applicationId);
      let questionCategories = await ApplicationQuestion.findAll({
        where: {
          applicationId: application.id
        },
        include: [{
          model: CategoryAnswer,
          as: 'categoryAnswers'
        }]
      });

      let applicationCategories = [];
      _.each(questionCategories, question => {
        applicationCategories = _.union(
          applicationCategories,
          _.map(question.categoryAnswers, category => {
            return category.categoryId;
          })
        );
      });

      let answers = [];
      _.each(questionCategories, question => {
        answers = _.union(
          answers,
          _.map(question.categoryAnswers, category => {
            return category.answer;
          })
        );
      });

      let vehicles = await Vehicle.findAll({
        include: [{
            model: Score,
            as: 'scores',
            where: {
              score: {
                [Op.lte]: application.finalScore
              }
            }
          },
          {
            model: VehicleCompany,
            as: 'vehicleCompanies',
            where: {
              vehicleTypeId: application.vehicleTypeId
            }
          },
          {
            model: Category,
            as: 'categories',
            where: {
              id: {
                [Op.in]: applicationCategories
              }
            }
          },
          {
            model: Picture,
            as: 'pictures'
          }
        ]
      });

      if (answers.includes(sedan)) {
        vehicles = vehicles.filter(vehicle => vehicle.categories.find(c => c.description == sedan));
      } else if (answers.includes(hatchback)) {
        vehicles = vehicles.filter(vehicle => vehicle.categories.find(c => c.description == hatchback));
      }

      if (application.vehicleTypeId != bikeId && !answers.includes(sedan) && !answers.includes(hatchback)) {
        if (answers.includes('Pequeño') && answers.includes('Rápido') && answers.includes('Ahorrador de gasolina')) {
          let bikes = await Vehicle.findAll({
            include: [{
                model: Score,
                as: 'scores',
                where: {
                  score: {
                    [Op.lte]: application.finalScore
                  }
                }
              },
              {
                model: VehicleCompany,
                as: 'vehicleCompanies',
                where: {
                  vehicleTypeId: bikeId
                }
              },
              {
                model: Category,
                as: 'categories',
                where: {
                  id: {
                    [Op.in]: applicationCategories
                  }
                }
              },
              {
                model: Picture,
                as: 'pictures'
              }
            ]
          });
          vehicles = _.union(bikes, vehicles);
        }
      }

      return res.ok(vehicles);
    } catch (e) {
      return res.negotiate(e);
    }
  },

  lowCost: async (req, res) => {
    let params = req.allParams();
    try {
      let vehicles = await Vehicle.findAll({
        include: [{
            model: Score,
            as: 'scores'
          },
          {
            model: VehicleCompany,
            as: 'vehicleCompanies',
            where: {
              vehicleTypeId: params.vehicleTypeId
            }
          },
          {
            model: Picture,
            as: 'pictures'
          }
        ],
        order: [
          ['scores', 'score', 'asc']
        ]
      });

      return res.ok(vehicles.slice(0, params.limit || 3));
    } catch (e) {
      return res.negotiate(e);
    }
  },

  likes: async (req, res) => {
    let params = req.allParams();

    try {
      let applicationVehicle = await ApplicationVehicle.findOrCreate({
        where: params,
        defaults: params
      });
      res.ok();
    } catch (e) {
      return res.negotiate(e);
    }
  }
};