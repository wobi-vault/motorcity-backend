/**
 * VehicleCategory.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    // vehicleId: {
    //   type: Sequelize.INTEGER,
    //   allowNull: false,
    //   primaryKey: true,
    //   references: {
    //     model: 'Vehicle',
    //     key: 'id'
    //   }
    // },
    // categoryId: {
    //   type: Sequelize.INTEGER,
    //   allowNull: false,
    //   primaryKey: true,
    //   references: {
    //     model: 'Category',
    //     key: 'id'
    //   }
    // }
  },

  associations: function() {},

  options: {
    tableName: 'VehicleCategory',
    classMethods: {},
    instanceMethods: {},
    hooks: {}
  }

};
