/**
 * VehicleCompany.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    url: {
      type: Sequelize.STRING(255),
      allowNull: false
    },
    minPrice: {
      type: Sequelize.REAL,
      allowNull: false
    },
    available: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: true
    }
  },

  associations: function() {
    // VehicleCompany.belongsTo(Vehicle, {
    //   foreignKey: {
    //     name: 'vehicleId',
    //     allowNull: false
    //   }
    // });
    // VehicleCompany.belongsTo(VehicleType, {
    //   foreignKey: {
    //     name: 'vehicleTypeId',
    //     allowNull: false
    //   }
    // });
    // VehicleCompany.belongsTo(Company, {
    //   foreignKey: {
    //     name: 'companyId',
    //     allowNull: false
    //   }
    // });
  },

  options: {
    tableName: 'VehicleCompany',
    classMethods: {},
    instanceMethods: {},
    hooks: {}
  }

};
