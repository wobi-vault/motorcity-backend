/**
 * VehicleMonthlyPayment.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {},

  associations: function() {
    VehicleMonthlyPayment.belongsTo(Vehicle, {
      as: 'vehicle',
      foreignKey: {
        name: 'vehicleId',
        allowNull: false
      }
    });
    VehicleMonthlyPayment.belongsTo(MonthlyPayment, {
      as: 'monthlyPayment',
      foreignKey: {
        name: 'monthlyPaymentId',
        allowNull: false
      }
    });
  },

  options: {
    tableName: 'VehicleMonthlyPayment',
    classMethods: {},
    instanceMethods: {},
    hooks: {}
  }
};


