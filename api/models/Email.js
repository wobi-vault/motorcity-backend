/**
 * Email.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    email: {
      type: Sequelize.STRING(60),
      allowNull: false
    },
    active: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: true
    }
  },

  associations: function() {
    Email.belongsTo(Company, {
      foreignKey: {
        name: 'companyId',
        allowNull: false
      }
    });
  },

  options: {
    tableName: 'Email',
    classMethods: {},
    instanceMethods: {},
    hooks: {}
  }

};
