/**
 * VehicleFeatureVehicle.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {},

  associations: function() {
    VehicleFeatureVehicle.belongsTo(Vehicle, {
      as: 'vehicle',
      foreignKey: {
        name: 'vehicleId',
        allowNull: false
      }
    });
    VehicleFeatureVehicle.belongsTo(VehicleFeature, {
      as: 'vehicleFeature',
      foreignKey: {
        name: 'vehicleFeatureId',
        allowNull: false
      }
    });
  },

  options: {
    tableName: 'VehicleFeatureVehicle',
    classMethods: {},
    instanceMethods: {},
    hooks: {}
  }
};

