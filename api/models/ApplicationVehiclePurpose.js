/**
 * ApplicationVehiclePurpose.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {},

  associations: function() {
    ApplicationVehiclePurpose.belongsTo(Application, {
      as: 'application',
      foreignKey: {
        name: 'applicationId',
        allowNull: false
      }
    });
    ApplicationVehiclePurpose.belongsTo(VehiclePurpose, {
      as: 'vehiclePurpose',
      foreignKey: {
        name: 'vehiclePurposeId',
        allowNull: true
      }
    });
  },

  options: {
    tableName: 'ApplicationVehiclePurpose',
    classMethods: {},
    instanceMethods: {},
    hooks: {}
  }
};
