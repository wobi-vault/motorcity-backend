/**
 * Brand.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    name: {
      type: Sequelize.STRING(60),
      allowNull: false
    }
  },

  associations: function() {},

  options: {
    tableName: 'Brand',
    classMethods: {},
    instanceMethods: {},
    hooks: {}
  }

};