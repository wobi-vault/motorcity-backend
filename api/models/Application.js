/**
 * Application.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    terms: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
    },
    privacy: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
    },
    partialScore: {
      type: Sequelize.REAL,
      allowNull: true,
    },
    finalScore: {
      type: Sequelize.REAL,
      allowNull: true,
    },
  },

  associations: function() {
    Application.belongsTo(Lead, {
      as: 'lead',
      foreignKey: {
        name: 'leadId',
        allowNull: false,
      },
    });
    Application.belongsTo(VehicleHitch, {
      as: 'vehicleHitch',
      foreignKey: {
        name: 'vehicleHitchId',
        allowNull: true,
      },
    });
    Application.belongsTo(VehicleMaxPrice, {
      as: 'vehicleMaxPrice',
      foreignKey: {
        name: 'vehicleMaxPriceId',
        allowNull: true,
      },
    });
    Application.belongsTo(MonthlyPayment, {
      as: 'monthlyPayment',
      foreignKey: {
        name: 'monthlyPaymentId',
        allowNull: true,
      },
    });
    Application.belongsTo(VehicleType, {
      as: 'vehicleType',
      foreignKey: {
        name: 'vehicleTypeId',
        allowNull: true,
      },
    });
    Application.belongsToMany(VehicleFeature, {
      through: ApplicationVehicleFeature,
      as: 'vehicleFeatures',
      foreignKey: 'applicationId'
    });
    Application.belongsToMany(VehiclePurpose, {
      through: ApplicationVehiclePurpose,
      as: 'vehiclePurposes',
      foreignKey: 'applicationId'
    });

    // Application.belongsToMany(Question, {
    //   through: ApplicationQuestion,
    //   as: 'questions',
    //   foreignKey: 'applicationId',
    // });
    Application.belongsTo(VehicleSize, {
      as: 'vehicleSize',
      foreignKey: {
        name: 'vehicleSizeId',
        allowNull: true,
      },
    });
    // Application.belongsToMany(Question, {
    //   through: ApplicationQuestion,
    //   as: 'questions',
    //   foreignKey: 'applicationId',
    // });
    // Application.belongsToMany(Vehicle, {
    //   through: ApplicationVehicle,
    //   as: 'vehicles',
    //   foreignKey: 'applicationId',
    // });
  },

  options: {
    tableName: 'Application',
    classMethods: {},
    instanceMethods: {},
    hooks: {},
  },
};
