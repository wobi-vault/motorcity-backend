/**
 * Urls.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    url: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    active: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: true,
    },
  },

  associations: function () {
    Urls.belongsToMany(Vehicle, {
      through: VehicleUrls,
      as: 'vehicles',
      foreignKey: 'urlId'
    });
  },

  options: {
    tableName: 'Urls',
    classMethods: {},
    instanceMethods: {},
    hooks: {},
  },
};

