/**
 * Lead.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    name: {
      type: Sequelize.STRING(120),
      allowNull: false
    },
    paternalSurname: {
      type: Sequelize.STRING(120),
      allowNull: false
    },
    maternalSurname: {
      type: Sequelize.STRING(120),
      allowNull: false
    },
    phone: {
      type: Sequelize.STRING(10),
      allowNull: false
    },
    email: {
      type: Sequelize.STRING(60),
      allowNull: false
    },
    reachable: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: true
    }
  },

  associations: function() {
    Lead.belongsTo(Age, {
      as: 'age',
      foreignKey: {
        name: 'ageId',
        allowNull: false,
      },
    });
    Lead.belongsTo(Profession, {
      as: 'profession',
      foreignKey: {
        name: 'professionId'
      },
    });
    Lead.belongsTo(State, {
      as: 'state',
      foreignKey: {
        name: 'stateId'
      },
    });
    Lead.belongsTo(Gender, {
      as: 'gender',
      foreignKey: {
        name: 'genderId',
        allowNull: false,
      },
    });
  },

  options: {
    tableName: 'Lead',
    classMethods: {},
    instanceMethods: {},
    hooks: {}
  }

};
