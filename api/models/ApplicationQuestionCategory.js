/**
 * ApplicationQuestionCategory.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    // applicationQuestionId: {
    //   type: Sequelize.INTEGER,
    //   allowNull: false,
    //   primaryKey: true,
    //   references: {
    //     model: 'ApplicationQuestion',
    //     key: 'id'
    //   }
    // },
    // categoryAnswerId: {
    //   type: Sequelize.INTEGER,
    //   allowNull: false,
    //   primaryKey: true,
    //   references: {
    //     model: 'CategoryAnswer',
    //     key: 'id'
    //   }
    // }
  },

  associations: function() {},

  options: {
    tableName: 'ApplicationQuestionCategory',
    classMethods: {},
    instanceMethods: {},
    hooks: {}
  }

};
