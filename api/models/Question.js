/**
 * Question.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    question: {
      type: Sequelize.STRING(120),
      allowNull: false
    },
    step: {
      type: Sequelize.INTEGER,
      allowNull: false
    },
    active: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: true
    }
  },

  associations: function() {
    // Question.belongsTo(QuestionType, {
    //   as: 'questionType',
    //   foreignKey: {
    //     name: 'questionTypeId',
    //     allowNull: false
    //   }
    // });
    // Question.belongsTo(VehicleType, {
    //   as: 'vehicleType',
    //   foreignKey: {
    //     name: 'vehicleTypeId',
    //     allowNull: false
    //   }
    // });
    // Question.belongsToMany(Application, {
    //   through: ApplicationQuestion,
    //   foreignKey: 'questionId'
    // });
    // Question.hasMany(ScoreAnswer, {
    //   as: 'scoreAnswers',
    //   foreignKey: 'questionId'
    // });
    // Question.hasMany(CategoryAnswer, {
    //   as: 'categoryAnswers',
    //   foreignKey: 'questionId'
    // });
  },

  options: {
    tableName: 'Question',
    classMethods: {},
    instanceMethods: {},
    hooks: {}
  }

};
