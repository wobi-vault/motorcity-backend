/**
 * VehicleSizeVehicle.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {},

  associations: function() {
    VehicleSizeVehicle.belongsTo(Vehicle, {
      as: 'vehicle',
      foreignKey: {
        name: 'vehicleId',
        allowNull: false
      }
    });
    VehicleSizeVehicle.belongsTo(VehicleSize, {
      as: 'vehicleSize',
      foreignKey: {
        name: 'vehicleSizeId',
        allowNull: false
      }
    });
  },

  options: {
    tableName: 'VehicleSizeVehicle',
    classMethods: {},
    instanceMethods: {},
    hooks: {}
  }
};