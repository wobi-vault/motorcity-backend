/**
 * ApplicationQuestion.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    // id: {
    //   type: Sequelize.INTEGER,
    //   autoIncrement: true,
    //   primaryKey: true
    // },
    // applicationId: {
    //   type: Sequelize.INTEGER,
    //   allowNull: false,
    //   references: {
    //     model: 'Application',
    //     key: 'id'
    //   }
    // },
    // questionId: {
    //   type: Sequelize.INTEGER,
    //   allowNull: false,
    //   references: {
    //     model: 'Question',
    //     key: 'id'
    //   }
    // }
  },

  associations: function() {
    // ApplicationQuestion.belongsToMany(CategoryAnswer, {
    //   through: ApplicationQuestionCategory,
    //   as: 'categoryAnswers',
    //   foreignKey: 'applicationQuestionId'
    // });
    // ApplicationQuestion.belongsToMany(ScoreAnswer, {
    //   through: ApplicationQuestionScore,
    //   as: 'scoreAnswers',
    //   foreignKey: 'applicationQuestionId'
    // });
  },

  options: {
    tableName: 'ApplicationQuestion',
    classMethods: {},
    instanceMethods: {},
    hooks: {}
  }

};
