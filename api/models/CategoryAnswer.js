/**
 * CategoryAnswer.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    answer: {
      type: Sequelize.STRING(60),
      allowNull: false
    },
    active: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: true
    }
  },

  associations: function() {
    // CategoryAnswer.belongsTo(Question, {
    //   foreignKey: {
    //     name: 'questionId',
    //     allowNull: false
    //   }
    // });
    // CategoryAnswer.belongsTo(Category, {
    //   foreignKey: {
    //     name: 'categoryId',
    //     allowNull: false
    //   }
    // });

    // CategoryAnswer.belongsToMany(ApplicationQuestion, {
    //   through: ApplicationQuestionCategory,
    //   as: 'applications',
    //   foreignKey: 'categoryAnswerId'
    // });
  },

  options: {
    tableName: 'CategoryAnswer',
    classMethods: {},
    instanceMethods: {},
    hooks: {}
  }

};
