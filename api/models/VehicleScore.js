/**
 * VehicleScore.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    // vehicleId: {
    //   type: Sequelize.INTEGER,
    //   allowNull: false,
    //   primaryKey: true,
    //   references: {
    //     model: 'Vehicle',
    //     key: 'id'
    //   }
    // },
    // scoreId: {
    //   type: Sequelize.INTEGER,
    //   allowNull: false,
    //   primaryKey: true,
    //   references: {
    //     model: 'Score',
    //     key: 'id'
    //   }
    // }
  },

  associations: function() {},

  options: {
    tableName: 'VehicleScore',
    classMethods: {},
    instanceMethods: {},
    hooks: {}
  }

};