/**
 * Profession.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    description: {
      type: Sequelize.STRING,
      allowNull: false,
      unique: true,
    },
    active: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: true,
    },
  },

  associations: function () {},

  options: {
    tableName: 'Profession',
    classMethods: {},
    instanceMethods: {},
    hooks: {},
  },
};
