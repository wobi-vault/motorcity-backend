/**
 * ApplicationVehicle.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    // applicationId: {
    //   type: Sequelize.INTEGER,
    //   allowNull: false,
    //   primaryKey: true,
    //   references: {
    //     model: 'Application',
    //     key: 'id'
    //   }
    // },
    // vehicleId: {
    //   type: Sequelize.INTEGER,
    //   allowNull: false,
    //   primaryKey: true,
    //   references: {
    //     model: 'Vehicle',
    //     key: 'id'
    //   }
    // }
  },

  associations: function() {},

  options: {
    tableName: 'ApplicationVehicle',
    classMethods: {},
    instanceMethods: {},
    hooks: {}
  }

};
