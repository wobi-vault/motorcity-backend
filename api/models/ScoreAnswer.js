/**
 * ScoreAnswer.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    answer: {
      type: Sequelize.STRING(60),
      allowNull: false
    },
    active: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: true
    }
  },

  associations: function() {
    // ScoreAnswer.belongsTo(Question, {
    //   as: 'question',
    //   foreignKey: {
    //     name: 'questionId',
    //     allowNull: false
    //   }
    // });
    // ScoreAnswer.belongsTo(Score, {
    //   as: 'score',
    //   foreignKey: {
    //     name: 'scoreId',
    //     allowNull: false
    //   }
    // });

    // ScoreAnswer.belongsToMany(ApplicationQuestion, {
    //   through: ApplicationQuestionScore,
    //   as: 'applications',
    //   foreignKey: 'scoreAnswerId'
    // });
  },

  options: {
    tableName: 'ScoreAnswer',
    classMethods: {},
    instanceMethods: {},
    hooks: {}
  }

};
