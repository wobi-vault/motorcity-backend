/**
 * VehicleMaxPriceVehicle.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {},

  associations: function() {
    VehicleMaxPriceVehicle.belongsTo(Vehicle, {
      as: 'vehicle',
      foreignKey: {
        name: 'vehicleId',
        allowNull: false
      }
    });
    VehicleMaxPriceVehicle.belongsTo(VehicleMaxPrice, {
      as: 'vehicleMaxPrice',
      foreignKey: {
        name: 'vehicleMaxPriceId',
        allowNull: false
      }
    });
  },

  options: {
    tableName: 'VehicleMaxPriceVehicle',
    classMethods: {},
    instanceMethods: {},
    hooks: {}
  }
};
