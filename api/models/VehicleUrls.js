/**
 * VehicleUrls.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {},

  associations: function() {
    VehicleUrls.belongsTo(Vehicle, {
      as: 'vehicle',
      foreignKey: {
        name: 'vehicleId',
        allowNull: false
      }
    });
    VehicleUrls.belongsTo(Urls, {
      as: 'url',
      foreignKey: {
        name: 'urlId',
        allowNull: false
      }
    });
  },

  options: {
    tableName: 'VehicleUrls',
    classMethods: {},
    instanceMethods: {},
    hooks: {}
  }
};


