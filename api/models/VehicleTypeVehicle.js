/**
 * VehicleTypeVehicle.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {},

  associations: function() {
    VehicleTypeVehicle.belongsTo(Vehicle, {
      as: 'vehicle',
      foreignKey: {
        name: 'vehicleId',
        allowNull: false
      }
    });
    VehicleTypeVehicle.belongsTo(VehicleType, {
      as: 'vehicleType',
      foreignKey: {
        name: 'vehicleTypeId',
        allowNull: false
      }
    });
  },

  options: {
    tableName: 'VehicleTypeVehicle',
    classMethods: {},
    instanceMethods: {},
    hooks: {}
  }
};



