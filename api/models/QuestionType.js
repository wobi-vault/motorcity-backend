/**
 * QuestionType.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    type: {
      type: Sequelize.STRING(60),
      allowNull: false
    }
  },

  associations: function() {},

  options: {
    tableName: 'QuestionType',
    classMethods: {},
    instanceMethods: {},
    hooks: {}
  }

};