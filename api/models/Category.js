/**
 * Category.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    description: {
      type: Sequelize.STRING(60),
      allowNull: false
    }
  },

  associations: function() {
    // Category.belongsToMany(Vehicle, {
    //   through: VehicleCategory,
    //   as: 'vehicles',
    //   foreignKey: 'categoryId'
    // });
  },

  options: {
    tableName: 'Category',
    classMethods: {},
    instanceMethods: {},
    hooks: {}
  }

};
