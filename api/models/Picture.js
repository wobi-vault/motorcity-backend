/**
 * Picture.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    name: {
      type: Sequelize.STRING(120),
      allowNull: false
    },
    location: {
      type: Sequelize.STRING(255),
      allowNull: false
    },
    contentType: {
      type: Sequelize.STRING(60),
      allowNull: false
    },
    visible: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: true
    }
  },

  associations: function() {
    Picture.belongsTo(Vehicle, {
      foreignKey: {
        name: 'vehicleId',
        allowNull: false
      }
    });
  },

  options: {
    tableName: 'Picture',
    classMethods: {},
    instanceMethods: {},
    hooks: {}
  }

};
