/**
 * Vehicle.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    model: {
      type: Sequelize.STRING(60),
      allowNull: false
    },
    description: {
      type: Sequelize.STRING(255),
      allowNull: true
    },
    performance: {
      type: Sequelize.STRING(120),
      allowNull: true
    },
    transmission: {
      type: Sequelize.STRING(120),
      allowNull: true
    },
    active: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
    }
  },

  associations: function() {
    Vehicle.hasMany(Picture, {
      as: 'pictures',
      foreignKey: 'vehicleId'
    });
    Vehicle.belongsToMany(VehicleFeature, {
      through: VehicleFeatureVehicle,
      as: 'vehicleFeatures',
      foreignKey: 'vehicleId'
    });
    Vehicle.belongsToMany(MonthlyPayment, {
      through: VehicleMonthlyPayment,
      as: 'vehiclePayments',
      foreignKey: 'vehicleId'
    });
    Vehicle.belongsToMany(VehicleType, {
      through: VehicleTypeVehicle,
      as: 'vehicleTypes',
      foreignKey: 'vehicleId'
    });
    Vehicle.belongsToMany(VehicleSize, {
      through: VehicleSizeVehicle,
      as: 'vehicleSizes',
      foreignKey: 'vehicleId'
    });
    Vehicle.belongsToMany(VehicleMaxPrice, {
      through: VehicleMaxPriceVehicle,
      as: 'vehicleMaxPrices',
      foreignKey: 'vehicleId'
    });
    Vehicle.belongsToMany(Urls, {
      through: VehicleUrls,
      as: 'vehicleUrls',
      foreignKey: 'vehicleId'
    });


    // Vehicle.belongsTo(Brand, {
    //   as: 'brand',
    //   foreignKey: {
    //     name: 'brandId',
    //     allowNull: true
    //   }
    // });
    // Vehicle.belongsToMany(Category, {
    //   through: VehicleCategory,
    //   as: 'categories',
    //   foreignKey: 'vehicleId'
    // });
    // Vehicle.belongsToMany(Score, {
    //   through: VehicleScore,
    //   as: 'scores',
    //   foreignKey: 'vehicleId'
    // });
    // Vehicle.belongsToMany(Application, {
    //   through: ApplicationVehicle,
    //   foreignKey: 'vehicleId'
    // });
    // Vehicle.hasMany(VehicleCompany, {
    //   as: 'vehicleCompanies',
    //   foreignKey: 'vehicleId'
    // });
  },

  options: {
    tableName: 'Vehicle',
    classMethods: {},
    instanceMethods: {},
    hooks: {}
  }

};
