/**
 * Company.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    name: {
      type: Sequelize.STRING(60),
      allowNull: false
    },
    available: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: true
    }
  },

  associations: function() {},

  options: {
    tableName: 'Company',
    classMethods: {},
    instanceMethods: {},
    hooks: {}
  }

};