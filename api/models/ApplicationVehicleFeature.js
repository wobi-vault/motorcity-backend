/**
 * ApplicationVehicleFeature.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {},

  associations: function() {
    ApplicationVehicleFeature.belongsTo(Application, {
      as: 'application',
      foreignKey: {
        name: 'applicationId',
        allowNull: false
      }
    });
    ApplicationVehicleFeature.belongsTo(VehicleFeature, {
      as: 'vehicleFeature',
      foreignKey: {
        name: 'vehicleFeatureId',
        allowNull: true
      }
    });
  },

  options: {
    tableName: 'ApplicationVehicleFeature',
    classMethods: {},
    instanceMethods: {},
    hooks: {}
  }
};

