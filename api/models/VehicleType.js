/**
 * VehicleType.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    type: {
      type: Sequelize.STRING(60),
      allowNull: false
    },
    active: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: true
    }
  },

  associations: function() {
    VehicleType.belongsToMany(Vehicle, {
      through: VehicleTypeVehicle,
      as: 'vehicles',
      foreignKey: 'vehicleTypeId'
    });
  },

  options: {
    tableName: 'VehicleType',
    classMethods: {},
    instanceMethods: {},
    hooks: {}
  }

};
