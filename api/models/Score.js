/**
 * Score.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    description: {
      type: Sequelize.STRING(120),
      allowNull: false
    },
    score: {
      type: Sequelize.INTEGER,
      allowNull: false
    }
  },

  associations: function() {
    // Score.belongsToMany(Vehicle, {
    //   through: VehicleScore,
    //   as: 'vehicles',
    //   foreignKey: 'scoreId'
    // });
  },

  options: {
    tableName: 'Score',
    classMethods: {},
    instanceMethods: {},
    hooks: {}
  }

};
