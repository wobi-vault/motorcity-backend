/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {
  '/': {
    view: 'pages/homepage'
  },
  '/registro': {
    view: 'pages/register'
  },
  '/cuestionario-1': {
    view: 'pages/step1'
  },
  '/cuestionario-2': {
    view: 'pages/step2'
  },
  '/cuestionario-3': {
    view: 'pages/step3'
  },
  '/resultados': {
    view: 'pages/step4'
  },
  // '/cuestionario': {
  //   view: 'pages/score'
  // },
  // '/categoria': {
  //   view: 'pages/category'
  // },
  // '/resultados': {
  //   view: 'pages/results'
  // },
  '/faq': {
    view: 'pages/faq'
  },
  '/terminos-y-condiciones': {
    view: 'pages/terms'
  },
  '/aviso-de-privacidad': {
    view: 'pages/privacy'
  },

  'POST /application/:applicationId/question/:questionId/score-answer/:scoreAnswerId': 'ApplicationController.scoreAnswer',
  'POST /application/:applicationId/question/:questionId/category-answer': 'ApplicationController.categoryAnswer',
  'GET /application/:applicationId/vehicle': 'VehicleController.fromApplication',
  'GET /vehicle/low-cost': 'VehicleController.lowCost',
  'POST /vehicle/:vehicleId/picture': 'PictureController.upload',
  'POST /application/:applicationId/vehicle/:vehicleId/like': 'VehicleController.likes',
  'POST /application/:applicationId/step2-lead': 'ReportController.step2Lead',
  'POST /report/email': 'ReportController.email',
  'POST /report/download': 'ReportController.download',
  'POST /report/:id/resend': 'ReportController.resend',
  'GET /report/columns': 'ReportController.columns',

  'PATCH /application/:applicationId/vehicle-feature': 'ApplicationController.vehicleFeature',
  'PATCH /application/:applicationId/vehicle-purpose': 'ApplicationController.vehiclePurpose',
  'GET /application/:applicationId/filter-result': 'ApplicationController.filterResult',
};
