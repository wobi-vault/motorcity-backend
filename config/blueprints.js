module.exports.blueprints = {

  actions: true,

  index: true,

  shortcuts: true,

  rest: true,

  prefix: '',

  restPrefix: '',

  pluralize: false,

  populate: true,

  defaultLimit: 30,

  populateLimit: 30,

  autoWatch: true,

}