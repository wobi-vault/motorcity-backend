module.exports.cron = {
  job: {
    schedule: '0 0 * * * *',
    onTick: async function() {
      if (sails.config.report.active) {
        const fs = require('fs');

        let now = new Date();
        let reports = await Report.findAll({
          where: {
            active: true,
            hour: now.getHours()
          }
        });

        let startDate = new Date(now);
        startDate.setMinutes(0);
        startDate.setSeconds(0);
        startDate.setMilliseconds(0);
        let endDate = new Date(startDate);
        endDate.setDate(startDate.getDate() - 1);

        _.each(reports, async (report) => {
          let columns = report.columns.split(',').map(c => {
            return c.trim();
          });
          let leads = await sails.helpers.getReportData(endDate.toLocaleString('es'), startDate.toLocaleString('es'), columns, report.repeatedData);
          let reportDoc = await sails.helpers.buildReport(leads, columns);
          let message = `Reporte del ${endDate.toLocaleString('es')}
        al ${startDate.toLocaleString('es')}.
        Total de leads nuevos: ${reportDoc.count}.`;
          if (!report.repeatedData) {
            message =
              message +
              '\n Nota: El reporte omite los leads repetidos, basado en su correo electrónico y teléfono.';
          }

          let attachment = (reportDoc.count) ? [reportDoc.path] : null;

          let inputs = {
            subject: report.subject || report.title,
            message: message,
            to: report.emails,
            attachments: attachment
          };

          await sails.helpers.sendEmail.with(inputs);
          console.log('Email sended');

          fs.unlink(reportDoc.path, err => {
            if (err) {
              console.log(reportDoc.path, ' wasn\'t deleted');
            } else {
              console.log(reportDoc.path, ' was deleted');
            }
          });
        });
      }
    }
  }
};