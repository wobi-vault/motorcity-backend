module.exports.models = {

  schema: true,

  attributes: {
    createdAt: {
      type: 'number',
      autoCreatedAt: true,
    },
    updatedAt: {
      type: 'number',
      autoUpdatedAt: true,
    },
    id: {
      type: 'number',
      autoIncrement: true,
    }
  },

  dataEncryptionKeys: {
    default: 'VnggEei2u6T1H55WjaAzuU7ENm+nlsvN3tRwPn4Rq3c='
  },

  cascadeOnDestroy: true

};