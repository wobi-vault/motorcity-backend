'use strict';
angular.module('app', [])
  .controller('results', [
    '$scope',
    '$http',
    function($scope, $http) {
      var vm = this;

      const lastScoreStep = 5;
      const lastCategoryStep = 8;
      const less15k = [64, 65, 66]; // DB ids from incomes less than 15k

      var scoreStep = sessionStorage.getItem('scoreStep');
      var categoryStep = sessionStorage.getItem('categoryStep');
      var vehicleTypeId = sessionStorage.getItem('vehicleTypeId');
      var leadId = sessionStorage.getItem('leadId');
      var applicationId = sessionStorage.getItem('applicationId');

      $scope.loading = true;
      $scope.active = false;
      $scope.topVehicles = [];
      $scope.allVehicles = [];

      if (scoreStep < lastScoreStep) {
        window.location = '../cuestionario';
      }

      if (categoryStep < lastCategoryStep) {
        window.location = '../categoria';
      }

      if (!applicationId || !leadId || !vehicleTypeId) {
        window.location = '..';
      }

      var sendEmail = function(lowCost) {
        var body = {
          vehicles: $scope.topVehicles,
          lowCost: lowCost,
          step2: false
        };
        $http.post('/application/' + applicationId + '/step2-lead', body).then(
          res => {
            if (!res.data.message.includes('not sent')) {
              // fbq('track', 'Lead');
            }
          },
          err => {
            console.error(err);
          }
        );
      };

      $http.get('/application/' + applicationId + '/vehicle').then(
        res => {
          var data = res.data.data;
          if (data.length) {
            for (var i = 0; i < 3; i++) {
              $scope.topVehicles.push(data.shift());
            }
            $scope.allVehicles = data;
            sessionStorage.setItem('finish', true);
            $scope.loading = false;
            if (!less15k.includes(parseInt(sessionStorage.getItem('incomeId')))) {
              sendEmail(false);
            }
          } else {
            $http.get('/application/' + applicationId).then(
              res => {
                var application = res.data.data;
                if (application.finalScore >= 2) {
                  $http
                    .get('/vehicle/low-cost/', {
                      params: {
                        vehicleTypeId
                      }
                    })
                    .then(
                      res => {
                        $scope.topVehicles = res.data.data;
                        sessionStorage.setItem('finish', true);
                        $scope.loading = false;
                        if (!less15k.includes(parseInt(sessionStorage.getItem('incomeId')))) {
                          sendEmail(true);
                        }
                      },
                      err => {
                        console.error(err);
                        Swal.fire({
                          title: 'Parece que ocurrió un problema',
                          text: 'Recarga la página o intenta de nuevo más tarde.',
                          type: 'error',
                          confirmButtonText: 'De acuerdo'
                        });
                      }
                    );
                } else {
                  sessionStorage.setItem('finish', true);
                  $scope.loading = false;
                }
              },
              err => {
                console.error(err);
                Swal.fire({
                  title: 'Parece que ocurrió un problema',
                  text: 'Recarga la página o intenta de nuevo más tarde.',
                  type: 'error',
                  confirmButtonText: 'De acuerdo'
                });
              }
            );
          }
        },
        err => {
          console.error(err);
          Swal.fire({
            title: 'Parece que ocurrió un problema',
            text: 'Recarga la página o intenta de nuevo más tarde.',
            type: 'error',
            confirmButtonText: 'De acuerdo'
          });
        }
      );

      $scope.like = function(vehicle) {
        $http
          .post(
            '/application/' + applicationId + '/vehicle/' + vehicle.id + '/like'
          )
          .then(
            res => {
              if (vehicle.vehicleCompanies[0].vehicleTypeId !== 2) {
                Swal.fire({
                  title: vehicle.model,
                  text: 'En breve nos pondremos en contacto contigo.',
                  type: 'success',
                  showConfirmButton: false,
                  timer: 2000
                });
              }
            },
            err => {
              console.error(err);
              Swal.fire({
                title: 'Parece que ocurrió un problema',
                text: 'Recarga la página o intenta de nuevo más tarde.',
                type: 'error',
                confirmButtonText: 'De acuerdo'
              });
            }
          );
      };
    }
  ]);