'use strict';
angular.module('app', []).controller('category', [
  '$scope',
  '$http',
  function($scope, $http) {
    var vm = this;

    const firstStep = 6;
    const lastStep = 8;

    var categoryAnswers = [];
    var step = sessionStorage.getItem('categoryStep') || firstStep;
    var vehicleTypeId = sessionStorage.getItem('vehicleTypeId');
    var leadId = sessionStorage.getItem('leadId');
    var applicationId = sessionStorage.getItem('applicationId');
    var finish = sessionStorage.getItem('finish');

    $scope.loading = true;

    if (!applicationId || !leadId || !vehicleTypeId || finish) {
      window.location = '..';
    }

    var getQuestion = function() {
      $scope.loading = true;
      var params = {
        vehicleTypeId,
        step
      };

      $http
        .get('/question', {
          params
        })
        .then(
          res => {
            categoryAnswers = [];
            $scope.step = step;
            $scope.question = res.data.data;
            $scope.loading = false;
          },
          err => {
            console.error(err);
            Swal.fire({
              title: 'Parece que ocurrió un problema',
              text: 'Recarga la página o intenta de nuevo más tarde.',
              type: 'error',
              confirmButtonText: 'De acuerdo'
            });
          }
        );
    };

    getQuestion();

    $scope.changeCategory = function(categoryId, active) {
      if (active) {
        categoryAnswers.push(categoryId);
      } else {
        let index = categoryAnswers.findIndex(a => {
          return a == categoryId;
        });
        categoryAnswers.splice(index, 1);
      }
    };

    $scope.answer = function() {
      if (categoryAnswers.length) {
        if (step >= lastStep) {
          window.location = '../resultados';
        }

        $scope.loading = true;
        $http
          .post(
            '/application/' +
              applicationId +
              '/question/' +
              $scope.question[0].id +
              '/category-answer',
            {
              categoryAnswers
            }
          )
          .then(
            res => {
              sessionStorage.setItem('categoryStep', parseInt(step) + 1);
              step = sessionStorage.getItem('categoryStep');
              getQuestion();
            },
            err => {
              console.error(err);
              Swal.fire({
                title: 'Parece que ocurrió un problema',
                text: 'Recarga la página o intenta de nuevo más tarde.',
                type: 'error',
                confirmButtonText: 'De acuerdo'
              });
            }
          );
      }
    };
  }
]);
