'use strict';
angular.module('app', []).controller('score', [
  '$scope',
  '$http',
  function($scope, $http) {
    var vm = this;

    const firstStep = 2;
    const lastStep = 5;
    const emailStep = 2;
    const less15k = [64, 65, 66];
    var step = sessionStorage.getItem('scoreStep') || firstStep;

    var leadId = sessionStorage.getItem('leadId');
    var applicationId = sessionStorage.getItem('applicationId');
    var finish = sessionStorage.getItem('finish');

    $scope.loading = true;
    $scope.score = {
      answer: 0
    };

    if (!applicationId || !leadId || finish) {
      window.location = '..';
    }

    if (step >= lastStep) {
      window.location = '../categoria';
    }

    var getQuestion = function() {
      $scope.loading = true;
      var params = {
        step
      };

      $http
        .get('/question', {
          params
        })
        .then(
          res => {
            $scope.step = step;
            $scope.question = res.data.data;
            $scope.score = {
              answer: 0
            };
            $scope.loading = false;
          },
          err => {
            console.error(err);
            Swal.fire({
              title: 'Parece que ocurrió un problema',
              text: 'Recarga la página o intenta de nuevo más tarde.',
              type: 'error',
              confirmButtonText: 'De acuerdo'
            });
          }
        );
    };

    getQuestion();

    var sendEmail = function() {
      var body = {
        income: $scope.score.answer,
        step2: true
      };
      $http.post('/application/' + applicationId + '/step2-lead', body).then(
        res => {
          if (!res.data.message.includes('not sent')) {
            // fbq('track', 'Lead');
          }
        },
        err => {
          console.error(err);
        }
      );
    };

    $scope.answer = function() {
      if ($scope.score.answer) {
        if (step >= lastStep) {
          window.location = '../categoria';
        }
        $scope.loading = true;
        $http
          .post(
            '/application/' +
            applicationId +
            '/question/' +
            $scope.question[0].id +
            '/score-answer/' +
            $scope.score.answer
          )
          .then(
            res => {
              if (parseInt(step) === emailStep && !less15k.includes(parseInt($scope.score.answer))) {
                sessionStorage.setItem('incomeId', parseInt($scope.score.answer));
                sendEmail();
              }
              sessionStorage.setItem('scoreStep', parseInt(step) + 1);
              step = sessionStorage.getItem('scoreStep');
              getQuestion();
            },
            err => {
              console.error(err);
              Swal.fire({
                title: 'Parece que ocurrió un problema',
                text: 'Recarga la página o intenta de nuevo más tarde.',
                type: 'error',
                confirmButtonText: 'De acuerdo'
              });
            }
          );
      }
    };
  }
]);