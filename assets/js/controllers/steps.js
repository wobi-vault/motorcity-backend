'use strict';

angular.module('app', [])
  .controller('step1', [
    '$scope',
    '$http',
    function($scope, $http) {
      var vm = this;

      const appId = sessionStorage.getItem('applicationId');
      const leadId = sessionStorage.getItem('leadId');

      if (!appId || !leadId) {
        window.location = '..';
      }

      $scope.loading = true;
      $scope.body = {
        vehicleHitchId: null,
        vehicleMaxPriceId: null,
        monthlyPaymentId: null,
        stateId: null,
        professionId: null
      };
      $scope.states = [];
      $scope.professions = [];

      const isFormValid = () =>
        !Object.values($scope.body).some(el => (el === undefined || el === null));

      $http.get('/vehicleHitch').then(
        result => {
          $scope.vehicleHitch = result.data.data;
        },
        err => {
          console.error(err);
        }
      );

      $http.get('/vehicleMaxPrice').then(
        result => {
          $scope.vehicleMaxPrice = result.data.data;
        },
        err => {
          console.error(err);
        }
      );

      $http.get('/monthlyPayment').then(
        result => {
          $scope.monthlyPayment = result.data.data;
        },
        err => {
          console.error(err);
        }
      );

      $http.get('/profession').then(
        result => {
          $scope.professions = result.data.data;
        },
        err => {
          console.error(err);
        }
      );

      $http.get('/state?order=name:asc').then(
        result => {
          $scope.states = result.data.data;
        },
        err => {
          console.error(err);
        }
      );

      $scope.handleSubmit = () => {
        if (isFormValid()) {
          const url = `/application/${appId}`;
          $http.patch(url, $scope.body).then(
            res => {
              $http.patch(`/lead/${leadId}`, $scope.body).then(
                res => {
                  $scope.loading = false;
                  window.location = '../cuestionario-2';
                },
                err => {
                  console.error(err);
                  Swal.fire({
                    title: 'Parece que ocurrió un problema',
                    text: 'Recarga la página o intenta de nuevo más tarde.',
                    type: 'error',
                    confirmButtonText: 'De acuerdo'
                  });
                }
              );
            },
            err => {
              console.error(err);
              Swal.fire({
                title: 'Parece que ocurrió un problema',
                text: 'Recarga la página o intenta de nuevo más tarde.',
                type: 'error',
                confirmButtonText: 'De acuerdo'
              });
            }
          );
        }
      };
    }
  ])
  .controller('step2', [
    '$scope',
    '$http',
    function($scope, $http) {
      let vm = this;

      const appId = sessionStorage.getItem('applicationId');
      const leadId = sessionStorage.getItem('leadId');

      if (!appId || !leadId) {
        window.location = '..';
      }

      $scope.purposes = [null];
      $scope.loading = true;
      $scope.body = {
        vehicleSizeId: null,
      };
      const isFormValid = () =>
        !Object.values($scope.body).some(el => (el === undefined || el === null));

      $http.get('/vehicleSize').then(
        result => {
          $scope.vehicleSize = result.data.data;
        },
        err => {
          console.error(err);
        }
      );

      $http.get('/vehiclePurpose').then(
        result => {
          $scope.vehiclePurpose = result.data.data;
        },
        err => {
          console.error(err);
        }
      );

      $scope.handleOnChange = function(id, active) {
        if ($scope.purposes[0] === null) {
          $scope.purposes.pop();
        }
        if (active) {
          $scope.purposes.push(id);
        } else {
          let index = $scope.purposes.findIndex(el => {
            return el === id;
          });
          $scope.purposes.splice(index, 1);
        }
      };

      $scope.handleSubmit = () => {
        if (isFormValid() && ($scope.purposes.length && $scope.purposes[0] !== null)) {
          let url = `/application/${appId}`;
          $http.patch(url, $scope.body).then(
            res => {
              url = `/application/${appId}/vehicle-purpose`;
              $http.patch(url, {
                answers: $scope.purposes
              }).then(
                res => {
                  $scope.loading = false;
                  window.location = '../cuestionario-3';
                },
                err => {
                  console.error(err);
                  Swal.fire({
                    title: 'Parece que ocurrió un problema',
                    text: 'Recarga la página o intenta de nuevo más tarde.',
                    type: 'error',
                    confirmButtonText: 'De acuerdo'
                  });
                }
              );
            },
            err => {
              console.error(err);
              Swal.fire({
                title: 'Parece que ocurrió un problema',
                text: 'Recarga la página o intenta de nuevo más tarde.',
                type: 'error',
                confirmButtonText: 'De acuerdo'
              });
            }
          );
        }
      };
    }
  ])
  .controller('step3', [
    '$scope',
    '$http',
    function($scope, $http) {
      let vm = this;

      const appId = sessionStorage.getItem('applicationId');
      const leadId = sessionStorage.getItem('leadId');

      if (!appId || !leadId) {
        window.location = '..';
      }

      $scope.features = [null];
      $scope.loading = true;
      $scope.body = {
        vehicleTypeId: null,
      };
      const isFormValid = () =>
        !Object.values($scope.body).some(el => (el === undefined || el === null));

      $http.get('/vehicleType').then(
        result => {
          $scope.vehicleType = result.data.data;
        },
        err => {
          console.error(err);
        }
      );

      $http.get('/vehicleFeature').then(
        result => {
          $scope.vehicleFeature = result.data.data;
        },
        err => {
          console.error(err);
        }
      );

      $scope.handleOnChange = function(id, active) {
        if ($scope.features[0] === null) {
          $scope.features.pop();
        }
        if (active) {
          $scope.features.push(id);
        } else {
          let index = $scope.features.findIndex(el => {
            return el === id;
          });
          $scope.features.splice(index, 1);
        }
      };

      $scope.handleSubmit = () => {
        if (isFormValid() && ($scope.features.length && $scope.features[0] !== null)) {
          let url = `/application/${appId}`;
          $http.patch(url, $scope.body).then(
            res => {
              url = `/application/${appId}/vehicle-feature`;
              $http.patch(url, {
                answers: $scope.features
              }).then(
                res => {
                  window.location = '../resultados';
                },
                err => {
                  console.error(err);
                  Swal.fire({
                    title: 'Parece que ocurrió un problema',
                    text: 'Recarga la página o intenta de nuevo más tarde.',
                    type: 'error',
                    confirmButtonText: 'De acuerdo'
                  });
                }
              );
            },
            err => {
              console.error(err);
              Swal.fire({
                title: 'Parece que ocurrió un problema',
                text: 'Recarga la página o intenta de nuevo más tarde.',
                type: 'error',
                confirmButtonText: 'De acuerdo'
              });
            }
          );
        }
      };
    }
  ])
  .controller('results', [
    '$scope',
    '$http',
    function($scope, $http) {
      const appId = sessionStorage.getItem('applicationId');
      const leadId = sessionStorage.getItem('leadId');
      const repeated = sessionStorage.getItem("repeated");
      const pixelForNewLead = 'https://leadgenios.net/pixel.track?CID=438618&p=img&MerchantReferenceID=';

      if (!appId || !leadId) {
        window.location = '..';
      }

      $scope.loading = true;
      $scope.foundVehicles = true;

      if (repeated == 0 || repeated == 1) {
        fbq('track', 'Lead');
        const img = document.createElement("img");
        img.src = `${pixelForNewLead}${leadId}`;
        var src = document.getElementById("container");
        src.appendChild(img);
        sessionStorage.removeItem('repeated');
      }

      $http.get(`/application/${appId}/filter-result`).then(
        res => {
          let vehicles = res.data.data.vehicles;
          if (!vehicles.length) {
            $scope.foundVehicles = false;
            $http.get('/vehicle?active=true').then(
              result => {
                vehicles = result.data.data;
                $scope.loading = false;
                $scope.vehicles = vehicles.map(vehicle => {
                  if (vehicle.description !== null) {
                    vehicle.description = vehicle.description.split('#');
                  }
                  return vehicle;
                });
              },
              err => {
                console.error(err);
              }
            );
          } else {
            $scope.vehicles = vehicles.map(vehicle => {
              if (vehicle.description !== null) {
                vehicle.description = vehicle.description.split('#');
              }
              return vehicle;
            });
            $scope.loading = false;
          }
        },
        err => {
          console.error(err);
        }
      );
    }
  ]);