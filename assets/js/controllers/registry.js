'use strict';
angular.module('app', [])
  .directive('limitTo', [
    function() {
      return {
        restrict: 'A',
        link: function(scope, elem, attrs) {
          var limit = parseInt(attrs.limitTo);
          angular.element(elem).on('keypress', function(e) {
            var key;
            if (e.which === null) {
              // IE
              key = e.keyCode;
            }
            if (e.which !== 0) {
              // all but IE
              key = e.which;
            }
            if (
              this.value.length === limit &&
              (key !== 8 && key !== 46 && key !== undefined)
            ) {
              e.preventDefault();
            }
          });
        }
      };
    }
  ])
  .controller('registry', [
    '$scope',
    '$http',
    function($scope, $http) {
      var vm = this;

      $scope.ages = [];
      $scope.genders = [];
      $scope.loading = false;
      $scope.valid = true;
      $scope.onlyNumbers = /^\d+$/;
      $scope.nameRegex = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ-\s]+$/;
      $scope.emailRegex = /^\w+([\.\+-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;

      $http.get('/age').then(
        result => {
          $scope.ages = result.data.data;
        },
        err => {
          console.error(err);
        }
      );

      $http.get('/gender').then(
        result => {
          $scope.genders = result.data.data;
        },
        err => {
          console.error(err);
        }
      );

      $scope.body = {
        name: '',
        paternalSurname: '',
        maternalSurname: '',
        phone: '',
        email: '',
        ageId: null,
        genderId: null,
        terms: null,
        privacy: null,
      };

      var leadId = sessionStorage.getItem('leadId');
      var applicationId = sessionStorage.getItem('applicationId');

      if (applicationId && leadId) {
        $http.get('/lead/' + leadId).then(
          res => {
            $scope.body = res.data.data;
          },
          err => {
            console.error(err);
            Swal.fire({
              title: 'Parece que ocurrió un problema',
              text: 'Recarga la página o intenta de nuevo más tarde.',
              type: 'error',
              confirmButtonText: 'De acuerdo'
            });
          }
        );
      }

      const invalidForm = el => (el === undefined || el === null || el === '');

      $scope.lead = function() {
        const isValid = !Object.values($scope.body).some(invalidForm);
        $scope.valid = isValid;
        if (isValid) {
          $scope.loading = true;
          if ($scope.body.id) {
            $http.patch('/lead/' + leadId).then(
              () => {
                window.location = '../cuestionario-1';
              },
              err => {
                console.error(err);
                Swal.fire({
                  title: 'Parece que ocurrió un problema',
                  text: 'Recarga la página o intenta de nuevo más tarde.',
                  type: 'error',
                  confirmButtonText: 'De acuerdo'
                });
              }
            );
          } else {
            $http.post('/lead', $scope.body).then(
              res => {
                sessionStorage.setItem(
                  'applicationId',
                  res.data.data.application.id
                );
                sessionStorage.setItem('leadId', res.data.data.id);
                sessionStorage.setItem('repeated', 0);
                window.location = '../cuestionario-1';
              },
              err => {
                console.error(err);
                Swal.fire({
                  title: 'Parece que ocurrió un problema',
                  text: 'Recarga la página o intenta de nuevo más tarde.',
                  type: 'error',
                  confirmButtonText: 'De acuerdo'
                });
              }
            );
          }
        }
      };
    }
  ]);